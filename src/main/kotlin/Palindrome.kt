
/**
 * Checks if a string is a palindrome by excluding '#' as a special character without any transformation of the chain
 * Examples:
 *      - "aba" is a palindrome
 *      - "abba" is a palindrome
 *      - "abc" is NOT a palindrome
 *      - "ab#a" is a palindrome
 *      - "a###bb##a#" is a palindrome
 *      - "a###b#da#" is NOT a palindrome
 */
object Palindrome {

    private const val HASH = '#'

    fun String.isPalindrome() : Boolean {
        if (length <= 1) {
            return true
        }

        val hashFreeLength = count { it != HASH }
        val halfHashFreeLength = hashFreeLength/2
        var index = 0
        var indexReverse = lastIndex
        var hashFreeIndex = 0

        while (hashFreeIndex < halfHashFreeLength) {
            val char = get(index++).takeIf { it != HASH } ?: continue

            var reverseChar: Char
            do {
                reverseChar = get(indexReverse--)
            } while (reverseChar == HASH && (hashFreeLength-1-hashFreeIndex) >= halfHashFreeLength)

            if (char != reverseChar) {
                return false
            }

            hashFreeIndex++
        }

        return true
    }
}

import Palindrome.isPalindrome
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class PalindromeTest {

    @Test
    fun `An empty string should be asserted as a palindrome`() {
        assertTrue("".isPalindrome())
    }

    @Test
    fun `"####" should be asserted as a palindrome`() {
        assertTrue("####".isPalindrome())
    }

    @Test
    fun `"a" should be asserted as a palindrome`() {
        assertTrue("a".isPalindrome())
    }

    @Test
    fun `"#a####" should be asserted a a palindrome`() {
        assertTrue("#a####".isPalindrome())
    }

    @Test
    fun `"aba" should be asserted as a palindrome`() {
        assertTrue("aba".isPalindrome())
    }

    @Test
    fun `"abba" should be asserted as a palindrome`() {
        assertTrue("abba".isPalindrome())
    }

    @Test
    fun `"ab#a# should be asserted as a palindrome`() {
        assertTrue("ab#a".isPalindrome())
    }

    @Test
    fun `"ab#b##a#" should be asserted as a palindrome`() {
        assertTrue("ab#b##a#".isPalindrome())
    }

    @Test
    fun `"abc" should NOT be asserted as a palindrome`() {
        assertFalse("abc".isPalindrome())
    }

    @Test
    fun `"abcd" should NOT be asserted as a palindrome`() {
        assertFalse("abcd".isPalindrome())
    }

    @Test
    fun `"a###b#da#" should NOT be asserted as a palindrome`() {
        assertFalse("a###b#da#".isPalindrome())
    }

    @Test
    fun `"racecar" should be asserted as a palindrome`() {
        assertTrue("racecar".isPalindrome())
    }

    @Test
    fun `"ra#ce#ca##r#" should be asserted as a palindrome`() {
        assertTrue("ra#ce#ca##r#".isPalindrome())
    }

    @Test
    fun `"radar" should be asserted as a palindrome`() {
        assertTrue("radar".isPalindrome())
    }

    @Test
    fun `"#ra#d#a##r#" should be asserted as a palindrome`() {
        assertTrue("#ra#d#a##r#".isPalindrome())
    }
}